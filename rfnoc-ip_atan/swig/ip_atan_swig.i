/* -*- c++ -*- */

#define IP_ATAN_API
#define ETTUS_API

%include "gnuradio.i"/*			*/// the common stuff

//load generated python docstrings
%include "ip_atan_swig_doc.i"
//Header from gr-ettus
%include "ettus/device3.h"
%include "ettus/rfnoc_block.h"
%include "ettus/rfnoc_block_impl.h"

%{
#include "ettus/device3.h"
#include "ettus/rfnoc_block_impl.h"
#include "ip_atan/ffiatan.h"
%}

%include "ip_atan/ffiatan.h"
GR_SWIG_BLOCK_MAGIC2(ip_atan, ffiatan);
