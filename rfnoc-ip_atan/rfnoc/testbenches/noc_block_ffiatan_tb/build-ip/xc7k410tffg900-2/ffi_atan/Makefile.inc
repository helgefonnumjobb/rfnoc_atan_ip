# 
# Copyright 2020 FFI
# 

include $(TOOLS_DIR)/make/viv_ip_builder.mak

LIB_IP_FFI_ATAN_SRCS = $(IP_BUILD_DIR)/ffi_atan/ffi_atan.xci

LIB_IP_FFI_ATAN_OUTS = $(addprefix $(IP_BUILD_DIR)/ffi_atan/, \
ffi_atan.xci.out \
synth/ffi_atan.vhd \
) 

$(LIB_IP_FFI_ATAN_SRCS) $(LIB_IP_FFI_ATAN_OUTS) : $(RFNOC_UTIL_DIR)/ip/ffi_atan/ffi_atan.xci
	$(call BUILD_VIVADO_IP,ffi_atan,$(ARCH),$(PART_ID),$(RFNOC_UTIL_DIR)/ip,$(IP_BUILD_DIR),0)
