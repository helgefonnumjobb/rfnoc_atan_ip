/* 
 * Copyright 2020 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

`timescale 1ns/1ps
`define NS_PER_TICK 1
`define NUM_TEST_CASES 5

`include "sim_exec_report.vh"
`include "sim_clks_rsts.vh"
`include "sim_rfnoc_lib.svh"

module noc_block_ffiatan_tb();
  `TEST_BENCH_INIT("noc_block_ffiatan",`NUM_TEST_CASES,`NS_PER_TICK);
  localparam BUS_CLK_PERIOD = $ceil(1e9/166.67e6);
  localparam CE_CLK_PERIOD  = $ceil(1e9/200e6);
  localparam NUM_CE         = 1;  // Number of Computation Engines / User RFNoC blocks to simulate
  localparam NUM_STREAMS    = 1;  // Number of test bench streams
  `RFNOC_SIM_INIT(NUM_CE, NUM_STREAMS, BUS_CLK_PERIOD, CE_CLK_PERIOD);
  `RFNOC_ADD_BLOCK(noc_block_ffiatan, 0);

  localparam SPP = 8; // Samples per packet
  
  ////////////////////////////////////////////////////////////
  //
  // The cordic ip is set up with 17 bits to a FIX17_15 cartesian input. 
  // The Ettus FIX16_15 is sign extended to 17 bit to fit the cordic 
  // input without removing any bits. 
  //
  // Cordic phase output is set to 18 bit and FIX18_15 scaled radians 
  // ranging [-1,1]. We have chosen to sign truncate the  
  // 18 bits to 16, effectively making the output a FIX16_15 like the   
  // Ettus format. The sign bit truncation makes a cordic 1 output 
  // turn a -1, but since this is the same phase: -pi = pi this should 
  // be ok, and the full 16 bit resolution of the Ettus format is utilized
  //
  ////////////////////////////////////////////////////////////
  
  localparam        [15:0] ZERO            = 16'd0;
  localparam        [15:0] ONE_SHORT       = 16'b0111_1111_1111_1111;  // Is sign extended to fix_17_15 =>    2**15-1/2**15, almost  1 
  localparam signed [15:0] MINUS_ONE_SHORT = 16'sb1000_0000_0000_0001; // sign ext. to fix_17_15        => -(2**15-1)/2**15  almost -1
        
  // Remember: Ettus' dataformat is {{re1,im1},{re0,im0}}. Step through unit circle from -pi to 3pi/4 in steps of pi/4 
  logic [3:0][63:0] samples;
  assign samples[0] = {{MINUS_ONE_SHORT, MINUS_ONE_SHORT},{MINUS_ONE_SHORT,            ZERO}}; // -3pi/4, -pi     
  assign samples[1] = {{ONE_SHORT      , MINUS_ONE_SHORT},{ZERO           , MINUS_ONE_SHORT}}; //  -pi/4, -pi/2
  assign samples[2] = {{ONE_SHORT      ,       ONE_SHORT},{ONE_SHORT      ,            ZERO}}; //   pi/4,   0    
  assign samples[3] = {{MINUS_ONE_SHORT,       ONE_SHORT},{ZERO           ,       ONE_SHORT}}; //  3pi/4,  pi/2
         
  //localparam        [15:0] PI        = 16'b0110_0100_1000_1000;        // fix_16_13
  //localparam signed [15:0] MINUS_PI  = 16'b1001_1011_0111_1000;        
  
  localparam        [15:0]       ONE = 16'b1000_0000_0000_0000;
  localparam signed [15:0] MINUS_ONE = 16'sb1000_0000_0000_0000;       // fix16_15 = -1
  
  
  logic [3:0][63:0] expected_value;                                                            // Scaled radians
  assign expected_value[0] = {{16'(MINUS_ONE*3/4), 16'd0},{16'(MINUS_ONE)  , 16'd0}};          // -3/4, -1 
  assign expected_value[1] = {{16'(MINUS_ONE/4)  , 16'd0},{16'(MINUS_ONE/2), 16'd0}};          // -1/4, -1/2
  assign expected_value[2] = {{16'(ONE/4)        , 16'd0},{16'd0           , 16'd0}};          //  1/4,   0 
  assign expected_value[3] = {{16'(ONE*3/4)      , 16'd0},{16'(ONE/2)      , 16'd0}};          //  3/4,  1/2
  
  localparam div = 32768.0;
  
  /********************************************************
  ** Verification
  ********************************************************/
  initial begin : tb_main
  
    logic signed [15:0] e0, e1, r0,r1;
    string s;
    logic [31:0] random_word;
    logic [63:0] readback;

    /********************************************************
    ** Test 1 -- Reset
    ********************************************************/
    `TEST_CASE_START("Wait for Reset");
    while (bus_rst) @(posedge bus_clk);
    while (ce_rst) @(posedge ce_clk);
    `TEST_CASE_DONE(~bus_rst & ~ce_rst);

    /********************************************************
    ** Test 2 -- Check for correct NoC IDs
    ********************************************************/
    `TEST_CASE_START("Check NoC ID");
    // Read NOC IDs
    tb_streamer.read_reg(sid_noc_block_ffiatan, RB_NOC_ID, readback);
    $display("Read ffiatan NOC ID: %16x", readback);
    `ASSERT_ERROR(readback == noc_block_ffiatan.NOC_ID, "Incorrect NOC ID");
    `TEST_CASE_DONE(1);

    /********************************************************
    ** Test 3 -- Connect RFNoC blocks
    ********************************************************/
    `TEST_CASE_START("Connect RFNoC blocks");
    `RFNOC_CONNECT(noc_block_tb,noc_block_ffiatan,SC16,SPP);
    `RFNOC_CONNECT(noc_block_ffiatan,noc_block_tb,SC16,SPP);
    `TEST_CASE_DONE(1);

    /********************************************************
    ** Test 4 -- Write / readback user registers
    ********************************************************/
    `TEST_CASE_START("Write / readback user registers");
    random_word = $random();
    tb_streamer.write_user_reg(sid_noc_block_ffiatan, noc_block_ffiatan.SR_TEST_REG_0, random_word);
    tb_streamer.read_user_reg(sid_noc_block_ffiatan, 0, readback);
    $sformat(s, "User register 0 incorrect readback! Expected: %0d, Actual %0d", readback[31:0], random_word);
    `ASSERT_ERROR(readback[31:0] == random_word, s);
    random_word = $random();
    tb_streamer.write_user_reg(sid_noc_block_ffiatan, noc_block_ffiatan.SR_TEST_REG_1, random_word);
    tb_streamer.read_user_reg(sid_noc_block_ffiatan, 1, readback);
    $sformat(s, "User register 1 incorrect readback! Expected: %0d, Actual %0d", readback[31:0], random_word);
    `ASSERT_ERROR(readback[31:0] == random_word, s);
    `TEST_CASE_DONE(1);

    /********************************************************
    ** Test 5 -- Test sequence
    ********************************************************/
    `TEST_CASE_START("Test sequence");
    fork
      begin
        cvita_payload_t send_payload;
        for (int i = 0; i < SPP/2; i++) begin
          send_payload.push_back(samples[i]);
        end
        tb_streamer.send(send_payload);
      end
      begin
        cvita_payload_t recv_payload;
        cvita_metadata_t md;
        tb_streamer.recv(recv_payload,md);
        for (int i = 0; i < SPP/2; i++) begin
          assign e0 = expected_value[i][31:16];
          assign e1 = expected_value[i][63:48];
          $cast(r0, recv_payload[i][31:16]);  // a little black magic here, only $cast worked with recv_payload!!
          $cast(r1, recv_payload[i][63:48]);
          $sformat(s, "Expected: %0f %0f, Received: %0f %0f", e1/div, e0/div, r1/div, r0/div); // fix16_15, div=2**15=32768.0
          $display(s); 
          $sformat(s, "Incorrect value received! Expected: %0f %0f, Received: %0f %0f", e1/div, e0/div, r1/div, r0/div);	
          `ASSERT_ERROR(recv_payload[i] == expected_value[i], s);
        end
      end
    join
    `TEST_CASE_DONE(1);
    `TEST_BENCH_DONE;

  end
endmodule
