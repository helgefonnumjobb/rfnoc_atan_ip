INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_IP_ATAN ip_atan)

FIND_PATH(
    IP_ATAN_INCLUDE_DIRS
    NAMES ip_atan/api.h
    HINTS $ENV{IP_ATAN_DIR}/include
        ${PC_IP_ATAN_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    IP_ATAN_LIBRARIES
    NAMES gnuradio-ip_atan
    HINTS $ENV{IP_ATAN_DIR}/lib
        ${PC_IP_ATAN_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(IP_ATAN DEFAULT_MSG IP_ATAN_LIBRARIES IP_ATAN_INCLUDE_DIRS)
MARK_AS_ADVANCED(IP_ATAN_LIBRARIES IP_ATAN_INCLUDE_DIRS)

